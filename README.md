# Properties of a triangle's circumcircle #
This IJ1 script calculates the properties of a triangle's [circumcircle](https://en.wikipedia.org/wiki/Circumscribed_circle) which is the circle whose circumference passes through the three points of the triangle.

Calculated are:

* Area of circle
* Coordinates of circumcentre
* Circumradius
* Menger Curvature (inverse of radius)

Written and Tested in [Fiji](http://fiji.sc) 64bit.

### Usage ###

* Download and update [Fiji](http://fiji.sc)
* Open the script in the Fiji Script Editor and hit run. A blank demo image will be opened and you will be asked to mark three points.
* Hit OK and the script will run, paramters will be written out to the log and an overlay will mark the circumcentre, original triangle and the circumcircle.


![Example Output](https://bytebucket.org/davemason/threepointcircumcircle/raw/ed9b6df20ae065444da65dff38cd813868debb06/Overlay.PNG)

### Acknowledgements and Licence ###

* The code was written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk)
* Provided under a [CCBY 4.0 Licence](https://creativecommons.org/licenses/by/4.0/) which allows use and modification as long as the original source is attributed.
